var pool = require('./databaseConfig.js');
var aplikasiSehatDB = {

    getTipskesehatan: function (tips_category, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_tipskesehatan where	tips_category = ? ORDER BY (tips_id) DESC';
                conn.query(sql,[tips_category], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }

};
module.exports = aplikasiSehatDB