var pool = require('./databaseConfig.js');
var aplikasiSehatDB = {

    getDiskusiByEmail: function (email, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_diskusi where email = ? ORDER BY (id_diskusi) DESC';
                conn.query(sql,[email], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    getDiskusi: function (callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_diskusi ORDER BY (id_diskusi) DESC';
                conn.query(sql, function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
    
,
    addDiskusi: function(id_diskusi, nama, email, tanggal, diskusi, foto, callback){
        pool.getConnection(function(err, conn){
            if (err){
                console.log(err);
                return callback(err, null);
            }
            else{
                console.log("Connected");
                var sql = 'INSERT INTO tb_diskusi (id_diskusi, nama, email, tanggal, diskusi, foto) values (?,?,?,?,?,?)';
                conn.query(sql, [id_diskusi, nama, email, tanggal, diskusi, foto,], function(err, result){
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    getComment: function (id_diskusi, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'SELECT * FROM tb_diskusi_reply where id_diskusi = ?';
                conn.query(sql,[id_diskusi], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
,
    addComment: function (diskusi_reply_id, diskusi_reply_nama, diskusi_reply_isi, diskusi_reply_foto, id_diskusi, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'INSERT INTO tb_diskusi_reply (diskusi_reply_id, diskusi_reply_nama, diskusi_reply_isi, diskusi_reply_foto, id_diskusi) values (?,?,?,?,?)';
                conn.query(sql,[diskusi_reply_id, diskusi_reply_nama, diskusi_reply_isi, diskusi_reply_foto, id_diskusi], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }   
,
    deleteDiskusi: function (id_diskusi, callback){
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log(err);
                return callback(err, null);
            }
            else {
                console.log("Connected");
                var sql = 'DELETE FROM tb_diskusi where id_diskusi = ? ';
                conn.query(sql,[id_diskusi], function (err, result) {
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    }
                    else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
};
module.exports = aplikasiSehatDB