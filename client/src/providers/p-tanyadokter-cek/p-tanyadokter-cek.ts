import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { tanyadokter } from '../../model/m_tanyadokter_cek';

@Injectable()
export class PTanyadokterCekProvider {
public tanya_dokter: tanyadokter[] = [];

  constructor(public http: Http) {  }

  getTanyadokter(usernama){
    return this.http.get("http://localhost:8081/api/tanyadokter/"+usernama).map((response: Response) => {
      let data = response.json();
      this.tanya_dokter = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  addTanyadokter(data){
    var url ="http://localhost:8081/api/tanyadokter";
    return this.http.post(url, data)
  }

}
