import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { tipskesehatan } from '../../model/m_tipskesehatan';


@Injectable()
export class PTipskesehatanProvider {
  public tips: tipskesehatan[] = [];

  constructor(public http: Http) {
    console.log('Hello PTipskesehatanProvider Provider');
  }

  getTips(tips_category: string){
    return this.http.get("http://localhost:8081/api/tips/" + tips_category).map((response: Response) => {
      let data = response.json();
      this.tips = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

}
