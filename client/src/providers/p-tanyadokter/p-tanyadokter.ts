import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { listdokter } from '../../model/m_tanyadokter';

@Injectable()
export class PTanyadokterProvider {
  public list_dokter: listdokter[] = [];
  constructor(public http: Http) {  }

  getListdokter(){
    return this.http.get("http://localhost:8081/api/listdokter").map((response: Response) => {
      let data = response.json();
      this.list_dokter = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  cariDokter(cariDokter){
    return this.http.get("http://localhost:8081/api/listdokter/"+cariDokter).map((response: Response) => {
      let data = response.json();
      this.list_dokter = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

}
