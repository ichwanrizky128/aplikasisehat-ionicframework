import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { artikelKesehatan } from '../../model/m_artikel';

@Injectable()
export class PArtikelProvider {
  private artikel: artikelKesehatan[] = [];

  constructor(public http: Http) {
    console.log('Hello PArtikelProvider Provider');
  }

  getArtikel(){
    return this.http.get("http://localhost:8081/api/artikel").map((response: Response) => {
      let data = response.json();
      this.artikel = data;
      return data;
    },
    (error) => console.log(error)
  );
  }
}
