import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { ForumDiskusi } from '../../model/m_forumdiskusi';

@Injectable()
export class PForumdiskusiProvider {
  public forumdiskusi: ForumDiskusi[] = [];
  constructor(public http: Http) {
    console.log('Hello PForumdiskusiProvider Provider');
  }

  getDiskusi(){
    return this.http.get("http://localhost:8081/api/diskusi").map((response: Response) => {
      let data = response.json();
      this.forumdiskusi = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  getDiskusiByEmail(email: string){
    return this.http.get("http://localhost:8081/api/diskusi/"+ email).map((response: Response) => {
      let data = response.json();
      this.forumdiskusi = data;
      return data;
    },
    (error) => console.log(error)
  );
  }

  addDiskusi(data){
    var url ="http://localhost:8081/api/diskusi";
    return this.http.post(url, data)
  }

  deleteDiskusi(id_diskusi){
    var url ="http://localhost:8081/api/deletepost/"+ id_diskusi;
    return this.http.delete(url);
  }
}
