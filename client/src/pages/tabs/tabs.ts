import { Component } from '@angular/core';
import { HomePage } from '../user_no/home/home';
import { HomeUserPage } from '../user_user/home-user/home-user';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomeUserPage;
  tab2Root = HomeUserPage;
  tab3Root = HomeUserPage;

  color: string = "green";

  constructor(public navParams: NavParams) {
    console.log(this.navParams.data);
  }
  
}
