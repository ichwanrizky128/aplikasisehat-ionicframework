import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ForumDiskusi } from '../../../model/m_forumdiskusi';
import { PForumdiskusiProvider } from '../../../providers/p-forumdiskusi/p-forumdiskusi';
import { NoForumdiskusiCommentPage } from '../no-forumdiskusi-comment/no-forumdiskusi-comment';


@IonicPage()
@Component({
  selector: 'page-no-forumdiskusi',
  templateUrl: 'no-forumdiskusi.html',
})
export class NoForumdiskusiPage {
  public forumdiskusi: ForumDiskusi[] = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public forumDiskusi: PForumdiskusiProvider) {
  }

  ngOnInit(){
    this.getDiskusi()
  }

  getDiskusi(){
    this.forumdiskusi = [];
    this.forumDiskusi.getDiskusi().subscribe((result) => {
      console.log(result);
      this.forumdiskusi = result;
    });
  }

  viewComment(item){
    let modal = this.modalCtrl.create(NoForumdiskusiCommentPage, item);
    modal.present();
  }

}
