import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoArtikelDetailPage } from './no-artikel-detail';

@NgModule({
  declarations: [
    NoArtikelDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(NoArtikelDetailPage),
  ],
})
export class NoArtikelDetailPageModule {}
