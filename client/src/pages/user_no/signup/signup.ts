import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { PUserProvider } from '../../../providers/p-user/p-user';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  public nama: string ="";
  public email: string ="";
  public password: string ="";
  public foto: string ="";
  public confirmPassword: string ="";
  @ViewChild('fileInput') fileInput;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public userProvider: PUserProvider, public alertCtrl: AlertController) {

  }

  addUser(){
   if (this.password == ""){
     const alert = this.alertCtrl.create({
      subTitle: 'Data Tidak Boleh Kosong !',
      buttons: ['OK']
     });
     alert.present();
   }

   else if (this.nama == ""){
      const alert = this.alertCtrl.create({
      subTitle: 'Data Tidak Boleh Kosong !',
      buttons: ['OK']
     });
     alert.present();
   }

   else if (this.email == ""){
      const alert = this.alertCtrl.create({
      subTitle: 'Data Tidak Boleh Kosong !',
      buttons: ['OK']
     });
     alert.present();
   }

   else if (this.password != this.confirmPassword){
      const alert = this.alertCtrl.create({
      subTitle: 'Password Tidak Sama !',
      buttons: ['OK']
     });
     alert.present();
   }

   else {
    var path = this.foto;
    var filename = path.replace(/^C:\\fakepath\\/, "");
    var data = {
      "user_id":"",
      "user_nama":this.nama,
      "user_email":this.email,
      "user_password":this.password,
      "user_img":filename,
      "user_hakakses":"user",
    }
    console.log(data);
    this.userProvider.addUser(data).subscribe((result) => {
      const loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 3000
      });
      loader.present();
      setTimeout(() => {
        this.navCtrl.pop();
      },3000); 
    });
   }
  }

  getImage(){
    this.fileInput.nativeElement.click();
  }

}
