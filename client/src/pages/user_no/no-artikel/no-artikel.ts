import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PArtikelProvider } from '../../../providers/p-artikel/p-artikel';
import { artikelKesehatan } from '../../../model/m_artikel';
import { NoArtikelDetailPage } from '../no-artikel-detail/no-artikel-detail';


@IonicPage()
@Component({
  selector: 'page-no-artikel',
  templateUrl: 'no-artikel.html',
})
export class NoArtikelPage {
  public artikel: artikelKesehatan[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public artikelKesehatan: PArtikelProvider) {
  }

  ngOnInit(){
   this.getArtikel();
  }

  getArtikel(){
    this.artikel = [];
    this.artikelKesehatan.getArtikel().subscribe((result) => {
      console.log(result);
      this.artikel = result;
    })
  }

  artikelDetail(item){
    this.navCtrl.push(NoArtikelDetailPage, item)
  }

}
