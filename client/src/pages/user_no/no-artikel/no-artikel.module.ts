import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoArtikelPage } from './no-artikel';

@NgModule({
  declarations: [
    NoArtikelPage,
  ],
  imports: [
    IonicPageModule.forChild(NoArtikelPage),
  ],
})
export class NoArtikelPageModule {}
