import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { forumdiskusi_comment } from '../../../model/m_forumdiskusi_comment';
import { PForumdiskusiCommentProvider } from '../../../providers/p-forumdiskusi-comment/p-forumdiskusi-comment'


@IonicPage()
@Component({
  selector: 'page-no-forumdiskusi-comment',
  templateUrl: 'no-forumdiskusi-comment.html',
})
export class NoForumdiskusiCommentPage {
  public forumdiskusi_comment: forumdiskusi_comment[] = [];
  private id: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public forumDiskusiComment: PForumdiskusiCommentProvider) {
  }

  ngOnInit(){
    this.getComment();
  }


  getComment(){
    var data = this.navParams.data;
    this.id = data.id_diskusi;

    this.forumdiskusi_comment = [];
    this.forumDiskusiComment.getComment(this.id).subscribe((result) => {
      console.log(result);
      this.forumdiskusi_comment = result;
    });
  }

  close(){
    this.navCtrl.pop();
  }
}
