import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoTipskesehatanPage } from './no-tipskesehatan';

@NgModule({
  declarations: [
    NoTipskesehatanPage,
  ],
  imports: [
    IonicPageModule.forChild(NoTipskesehatanPage),
  ],
})
export class NoTipskesehatanPageModule {}
