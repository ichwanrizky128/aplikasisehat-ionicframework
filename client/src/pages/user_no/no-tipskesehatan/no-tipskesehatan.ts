import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { tipskesehatan } from '../../../model/m_tipskesehatan';
import { PTipskesehatanProvider } from '../../../providers/p-tipskesehatan/p-tipskesehatan';
import { NoTipskesehatanDetailPage } from '../no-tipskesehatan-detail/no-tipskesehatan-detail';

@IonicPage()
@Component({
  selector: 'page-no-tipskesehatan',
  templateUrl: 'no-tipskesehatan.html',
})
export class NoTipskesehatanPage {
  public tips: tipskesehatan[] = [];
  categorySelected = "diet";

  constructor(public navCtrl: NavController, public navParams: NavParams, public tipsKesehatan: PTipskesehatanProvider) {
  }

  ngOnInit(){
    this.getTips();
  }

  getTips(){
    this.tips = [];
    this.tipsKesehatan.getTips(this.categorySelected).subscribe((result) => {
      console.log(result);
      this.tips = result;
    })
  }

  tipsDetail(item){
    this.navCtrl.push(NoTipskesehatanDetailPage, item);
  }
}
