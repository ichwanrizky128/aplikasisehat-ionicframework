import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { User } from '../../../model/m_user';
import { PUserProvider } from '../../../providers/p-user/p-user';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import { HomeUserPage } from '../../user_user/home-user/home-user';
import { TabsPage } from '../../tabs/tabs';
import { HomeUserdokterPage } from '../../user_user/home-userdokter/home-userdokter';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public user: User[]= [];
  public username: string = "";
  public password: string = "";


  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public userProvider: PUserProvider, public alertCtrl: AlertController) {
  }

  signupclick(){
    this.navCtrl.push(SignupPage);
  }

  tesUser(){
    this.navCtrl.push(HomeUserPage);
  }

  login(){
    if(this.username == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Email Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else if(this.password == ""){
      const alert = this.alertCtrl.create({
        subTitle: 'Password Tidak Boleh Kosong !',
        buttons: ['OK']
       });
       alert.present();
    }

    else {
      this.user = [];
      this.userProvider.getUser2(this.username, this.password).subscribe((result) => {
        if (result.length == 0){
          const alert = this.alertCtrl.create({
            subTitle: 'Email Atau Password Salah !',
            buttons: ['OK']
           });
           alert.present();
        }
        else{
          for (let login of result){
            if (login.user_hakakses == "admin"){
              console.log("login sebagai admin");
            }
            else if (login.user_hakakses == "user"){
              const loader = this.loadingCtrl.create({
                content: "Please wait...",
                duration: 3000
              });
              loader.present();
              setTimeout(() => {
                console.log("login sebagai user");
                var data = {
                  "user_id": login.user_id,
                  "user_img": login.user_img
                }
                this.navCtrl.setRoot(HomeUserPage, data);     
              },3000);
            }
            else if (login.user_hakakses == "dokter"){
              const loader = this.loadingCtrl.create({
                content: "Please wait...",
                duration: 3000
              });
              loader.present();
              setTimeout(() => {
                console.log("login sebagai Dokter");
                var data = {
                  "user_id": login.user_id,
                  "user_img": login.user_img
                }
                this.navCtrl.setRoot(HomeUserdokterPage, data);     
              },3000);
            }
          }
        }
      });
    }
  }
}
