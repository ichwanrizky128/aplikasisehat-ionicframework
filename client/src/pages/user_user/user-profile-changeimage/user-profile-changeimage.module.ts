import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfileChangeimagePage } from './user-profile-changeimage';

@NgModule({
  declarations: [
    UserProfileChangeimagePage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfileChangeimagePage),
  ],
})
export class UserProfileChangeimagePageModule {}
