import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTipskesehatanDetailPage } from './user-tipskesehatan-detail';

@NgModule({
  declarations: [
    UserTipskesehatanDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTipskesehatanDetailPage),
  ],
})
export class UserTipskesehatanDetailPageModule {}
