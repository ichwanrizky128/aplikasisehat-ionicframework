import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserProfiledokterPage } from './user-profiledokter';

@NgModule({
  declarations: [
    UserProfiledokterPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfiledokterPage),
  ],
})
export class UserProfiledokterPageModule {}
