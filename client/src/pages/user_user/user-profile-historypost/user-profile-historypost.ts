import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController  } from 'ionic-angular';
import { ForumDiskusi } from '../../../model/m_forumdiskusi';
import { PForumdiskusiProvider } from '../../../providers/p-forumdiskusi/p-forumdiskusi';
import { PUserProvider } from '../../../providers/p-user/p-user';
import { User } from '../../../model/m_user';
import { UserForumdiskusiCommentPage } from '../user-forumdiskusi-comment/user-forumdiskusi-comment';


@IonicPage()
@Component({
  selector: 'page-user-profile-historypost',
  templateUrl: 'user-profile-historypost.html',
})
export class UserProfileHistorypostPage {
  public forumdiskusi: ForumDiskusi[] = [];
  public user: User[]= [];
  private user_email: string = "";
  private user_id: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public forumDiskusi: PForumdiskusiProvider,  public userProvider: PUserProvider, public modalCtrl: ModalController, public alertCtrl: AlertController) {
    var data = this.navParams.data
    this.user_email = data.user_email;
    this.user_id = data.user_id;

  }

  ngOnInit(){
    this.getDiskusi();
  }

  getDiskusi(){
    this.forumdiskusi = [];
    this.forumDiskusi.getDiskusiByEmail(this.user_email).subscribe((result) => {
      console.log(result);
      this.forumdiskusi = result;
    });
  }

  viewComment(item){
    this.user = [];
    this.userProvider.getUser3(this.user_id).subscribe((result) => {
      for(let data of result){
        var data2 = {
          "id_diskusi": item.id_diskusi,
          "nama": data.user_nama,
          "foto": data.user_img,
        }
    
        let modal = this.modalCtrl.create(UserForumdiskusiCommentPage, data2);
        modal.present();
      } 
    });   
  }

  deletPost(id_diskusi){
    const confirm = this.alertCtrl.create({
      title: 'Delete Post?',
      message: 'Do you agree to delete this post ?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.forumDiskusi.deleteDiskusi(id_diskusi).subscribe((result) => {
              this.getDiskusi();
            });
          }
        }
      ]
    });
    confirm.present();
  }

  close(){
    this.navCtrl.pop();
  }
}
