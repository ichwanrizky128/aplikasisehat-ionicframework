import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PUserProvider } from '../../../providers/p-user/p-user';


@IonicPage()
@Component({
  selector: 'page-user-profile-edit',
  templateUrl: 'user-profile-edit.html',
})
export class UserProfileEditPage {
  private user_nama: string = "";
  private user_email: string = "";
  private user_img: string = "";
  private user_id:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public updateProfileProvider: PUserProvider) {
    var data = this.navParams.data
    this.user_id = data.user_id;
    this.user_nama = data.user_nama;
    this.user_email = data.user_email;
    this.user_img = data.user_img;
  }

  close(){
    this.navCtrl.pop();
  }

  updateUser(){
    var data = {
      "user_nama": this.user_nama,
      "user_email": this.user_email
    }
    this.updateProfileProvider.updateProfile(this.user_id, data).subscribe((result) => {
      console.log(result);
      this.navCtrl.pop();
    });

  }

}
