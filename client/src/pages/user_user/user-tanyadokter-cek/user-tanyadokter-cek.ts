import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PTanyadokterCekProvider } from '../../../providers/p-tanyadokter-cek/p-tanyadokter-cek';
import { tanyadokter } from '../../../model/m_tanyadokter_cek';

@IonicPage()
@Component({
  selector: 'page-user-tanyadokter-cek',
  templateUrl: 'user-tanyadokter-cek.html',
})
export class UserTanyadokterCekPage {
  public tanya_dokter: tanyadokter[] = [];
  private user_nama: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public tanyadokterProvider: PTanyadokterCekProvider) {

  }

  ngOnInit(){
    this.getTanya();
  }

  getTanya(){
    var data = this.navParams.data
    this.user_nama = data.user_nama;
    this.tanya_dokter = [];
    this.tanyadokterProvider.getTanyadokter(this.user_nama).subscribe((result) => {
      console.log(result);
      this.tanya_dokter = result;
    });
  }

  close(){
    this.navCtrl.pop();
  }

}
