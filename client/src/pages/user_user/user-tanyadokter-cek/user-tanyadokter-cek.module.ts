import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTanyadokterCekPage } from './user-tanyadokter-cek';

@NgModule({
  declarations: [
    UserTanyadokterCekPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTanyadokterCekPage),
  ],
})
export class UserTanyadokterCekPageModule {}
