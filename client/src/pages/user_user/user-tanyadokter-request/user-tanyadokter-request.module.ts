import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTanyadokterRequestPage } from './user-tanyadokter-request';

@NgModule({
  declarations: [
    UserTanyadokterRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTanyadokterRequestPage),
  ],
})
export class UserTanyadokterRequestPageModule {}
