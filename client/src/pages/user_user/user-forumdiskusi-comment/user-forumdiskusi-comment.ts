import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { forumdiskusi_comment } from '../../../model/m_forumdiskusi_comment';
import { PForumdiskusiCommentProvider } from '../../../providers/p-forumdiskusi-comment/p-forumdiskusi-comment'

@IonicPage()
@Component({
  selector: 'page-user-forumdiskusi-comment',
  templateUrl: 'user-forumdiskusi-comment.html',
})
export class UserForumdiskusiCommentPage {
  public forumdiskusi_comment: forumdiskusi_comment[] = [];
  private id: string = "";
  private isiComment: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public forumDiskusiComment: PForumdiskusiCommentProvider) {
  }

  ngOnInit(){
    this.getComment();
  }


  getComment(){
    var data = this.navParams.data;
    this.id = data.id_diskusi;

    this.forumdiskusi_comment = [];
    this.forumDiskusiComment.getComment(this.id).subscribe((result) => {
      this.forumdiskusi_comment = result;
    });
  }

  comment(){
    var data = this.navParams.data;
    console.log(data);
    var data2 = {
      "diskusi_reply_id": "",
      "diskusi_reply_nama": data.nama,
      "diskusi_reply_isi": this.isiComment,
      "diskusi_reply_foto": data.foto,
      "id_diskusi": data.id_diskusi,
    }
    this.forumDiskusiComment.addComment(data2).subscribe((result) => {
      console.log(result);
      this.getComment();
    });
  }

  close(){
    this.navCtrl.pop();
  }

}
