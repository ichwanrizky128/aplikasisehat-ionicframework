import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserForumdiskusiCommentPage } from './user-forumdiskusi-comment';

@NgModule({
  declarations: [
    UserForumdiskusiCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(UserForumdiskusiCommentPage),
  ],
})
export class UserForumdiskusiCommentPageModule {}
