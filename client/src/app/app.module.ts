import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
/**Providers */
import { PForumdiskusiProvider } from '../providers/p-forumdiskusi/p-forumdiskusi';
import { PForumdiskusiCommentProvider } from '../providers/p-forumdiskusi-comment/p-forumdiskusi-comment';
import { PTanyadokterProvider } from '../providers/p-tanyadokter/p-tanyadokter';
import { PTipskesehatanProvider } from '../providers/p-tipskesehatan/p-tipskesehatan';
import { PUserProvider } from '../providers/p-user/p-user';
import { PArtikelProvider } from '../providers/p-artikel/p-artikel'

/**No User */
import { HomePage } from '../pages/user_no/home/home';
import { LoginPage } from '../pages/user_no/login/login';
import { NoTipskesehatanPage } from '../pages/user_no/no-tipskesehatan/no-tipskesehatan';
import { NoForumdiskusiPage } from '../pages/user_no/no-forumdiskusi/no-forumdiskusi';
import { NoTanyadokterPage } from '../pages/user_no/no-tanyadokter/no-tanyadokter';
import { NoForumdiskusiCommentPage } from '../pages/user_no/no-forumdiskusi-comment/no-forumdiskusi-comment';
import { NoTipskesehatanDetailPage } from '../pages/user_no/no-tipskesehatan-detail/no-tipskesehatan-detail';
import { SignupPage } from '../pages/user_no/signup/signup';
import { NoArtikelPage } from '../pages/user_no/no-artikel/no-artikel';
import { NoArtikelDetailPage } from '../pages/user_no/no-artikel-detail/no-artikel-detail';

/**User */
import { HomeUserPage } from '../pages/user_user/home-user/home-user';
import { UserForumdiskusiPage } from '../pages/user_user/user-forumdiskusi/user-forumdiskusi';
import { UserForumdiskusiCommentPage } from '../pages/user_user/user-forumdiskusi-comment/user-forumdiskusi-comment';
import { UserTanyadokterPage } from '../pages/user_user/user-tanyadokter/user-tanyadokter';
import { UserTipskesehatanPage } from '../pages/user_user/user-tipskesehatan/user-tipskesehatan';
import { UserTipskesehatanDetailPage } from '../pages/user_user/user-tipskesehatan-detail/user-tipskesehatan-detail';
import { UserProfilePage } from '../pages/user_user/user-profile/user-profile';
import { UserArtikelPage } from '../pages/user_user/user-artikel/user-artikel';
import { UserArtikelDetailPage } from '../pages/user_user/user-artikel-detail/user-artikel-detail';
import { UserProfileEditPage } from '../pages/user_user/user-profile-edit/user-profile-edit';
import { UserProfileChangepasswordPage } from '../pages/user_user/user-profile-changepassword/user-profile-changepassword';
import { UserProfileChangeimagePage } from '../pages/user_user/user-profile-changeimage/user-profile-changeimage';
import { UserProfileHistorypostPage } from '../pages/user_user/user-profile-historypost/user-profile-historypost';
import { UserTanyadokterRequestPage } from '../pages/user_user/user-tanyadokter-request/user-tanyadokter-request';
import { UserTanyadokterCekPage } from '../pages/user_user/user-tanyadokter-cek/user-tanyadokter-cek';
import { HomeUserdokterPage } from '../pages/user_user/home-userdokter/home-userdokter';
import { UserProfiledokterPage } from '../pages/user_user/user-profiledokter/user-profiledokter';

/**Tabs */
import { TabsPage } from '../pages/tabs/tabs';
import { PTanyadokterCekProvider } from '../providers/p-tanyadokter-cek/p-tanyadokter-cek';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    NoTipskesehatanPage,
    NoForumdiskusiPage,
    NoTanyadokterPage,
    NoForumdiskusiCommentPage,
    NoTipskesehatanDetailPage,
    SignupPage,
    HomeUserPage,
    TabsPage,
    UserForumdiskusiPage,
    UserForumdiskusiCommentPage,
    UserTanyadokterPage,
    UserTipskesehatanPage,
    UserTipskesehatanDetailPage,
    UserProfilePage,
    NoArtikelPage,
    NoArtikelDetailPage,
    UserArtikelPage,
    UserArtikelDetailPage,
    UserProfileEditPage,
    UserProfileChangepasswordPage,
    UserProfileChangeimagePage,
    UserProfileHistorypostPage,
    UserTanyadokterRequestPage,
    UserTanyadokterCekPage,
    HomeUserdokterPage,
    UserProfiledokterPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    NoTipskesehatanPage,
    NoForumdiskusiPage,
    NoTanyadokterPage,
    NoForumdiskusiCommentPage,
    NoTipskesehatanDetailPage,
    SignupPage,
    HomeUserPage,
    TabsPage,
    UserForumdiskusiPage,
    UserForumdiskusiCommentPage,
    UserTanyadokterPage,
    UserTipskesehatanPage,
    UserTipskesehatanDetailPage,
    UserProfilePage,
    NoArtikelPage,
    NoArtikelDetailPage,
    UserArtikelPage,
    UserArtikelDetailPage,
    UserProfileEditPage,
    UserProfileChangepasswordPage,
    UserProfileChangeimagePage,
    UserProfileHistorypostPage,
    UserTanyadokterRequestPage,
    UserTanyadokterCekPage,
    HomeUserdokterPage,
    UserProfiledokterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PForumdiskusiProvider,
    PForumdiskusiCommentProvider,
    PTanyadokterProvider,
    PTipskesehatanProvider,
    PUserProvider,
    PArtikelProvider,
    PTanyadokterCekProvider
  ]
})
export class AppModule {}
